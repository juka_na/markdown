# Powiększony nagłówek

*Lorem ipsum dolor sit amet,* **consectetur adipiscing elit**, ~~sed do eiusmod tempor incididunt ut labore~~ et dolore magna aliqua.

Fames ac turpis egestas sed tempus urna et. Mattis vulputate enim nulla aliquet porttitor lacus luctus. Id ornare arcu odio ut.

Fames ac turpis egestas sed tempus urna et. Mattis vulputate enim nulla aliquet porttitor lacus luctus. Id ornare arcu odio ut.

>Morbi leo urna molestie at elementum eu facilisis sed. Diam maecenas sed enim ut sem viverra.

Zagnieżdżoną listę numeryczną
1. Pierwszy główny punkt
   1. Zagnieżdżony punkt
   2. Kolejny zagnieżdżony punkt
      1. Głębiej zagnieżdżony punkt
2. Dwa
3. Trzy

Zagnieżdżoną listę nieuporządkowaną
- Jeden
- Dwa
- Trzy
    - trzy i pół

```
x = int(input(''))
for i in range(x):
    print('lorem')
```

To jest przykład tekstu z zagnieżdżonym kodem `x = 30`.

![apple.jpg](apple.jpg)